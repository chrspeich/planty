#!/usr/bin/env python

from miflora import miflora_scanner
from btlewrap.bluepy import BluepyBackend

def scan():
    """Scan for sensors."""
    print("Scanning for 10 seconds...")
    devices = miflora_scanner.scan(BluepyBackend, 10)
    print("Found {} devices:".format(len(devices)))
    for device in devices:
        print(f"  {device}")

scan()
