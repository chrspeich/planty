#!/usr/bin/env python3

import sys
import yaml
from time import sleep
from datetime import datetime, timedelta
import logging
import requests

from miflora.miflora_poller import (
    MI_BATTERY,
    MI_CONDUCTIVITY,
    MI_LIGHT,
    MI_MOISTURE,
    MI_TEMPERATURE,
    MiFloraPoller,
)
from btlewrap.bluepy import BluepyBackend

logging.basicConfig()
log = logging.getLogger("planty-agent")

class Sensor:
    def __init__(self, conf, agent):
        self._poller = MiFloraPoller(conf["mac"], BluepyBackend)
        self._plant_id = conf["id"]
        self._next_fetch = datetime.now()
        self._agent = agent

    def should_fetch(self):
        return (self._next_fetch < datetime.now())

    def process(self):
        self._poller.fill_cache()

        info = {
            "light": self._poller.parameter_value(MI_LIGHT),
            "temp": self._poller.parameter_value(MI_TEMPERATURE),
            "soil_moist": self._poller.parameter_value(MI_MOISTURE),
            "soil_ec": self._poller.parameter_value(MI_CONDUCTIVITY),
            "battery": self._poller.parameter_value(MI_BATTERY),
        }

        requests.post(f"{self._agent._planty}/plant/{self._plant_id}/ingest", json=info)
        print(info)


class Agent:
    def __init__(self, conf_path):
        with open(conf_path) as f:
            conf = yaml.safe_load(f)

        self._sensors = []
        for sensor_conf in conf["sensors"]:
            self._sensors.append(Sensor(sensor_conf, self))

        self._planty = conf["planty"]

    def run(self):
        pending = list(self._sensors)
        i = 10

        while pending and i > 0:
            sensors = pending
            pending = list()
            for sensor in sensors:
                if not sensor.should_fetch():
                    continue

                try:
                    sensor.process()
                except:
                    log.exception("Error processes")
                    pending.append(sensor)

            if pending:
                sleep(10)

            i -= 1

agent = Agent(sys.argv[1])

agent.run()
