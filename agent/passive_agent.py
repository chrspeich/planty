#!/usr/bin/env python3

from typing import Optional, Dict
from bluepy.btle import Scanner, DefaultDelegate, BTLEException
import sys
import struct
from dataclasses import dataclass, field
import yaml
import requests

T_STRUCT = struct.Struct("<h")
CND_STRUCT = struct.Struct("<H")
ILL_STRUCT = struct.Struct("<I")

def extract(data, length):
    return data[:length], data[length:]

def unpack_le16(data):
    buf, data = extract(data, 2)

    return ((buf[1] << 8) | buf[0]), data

def unpack_u8(data):
    buf, data = extract(data, 1)

    return buf[0], data


@dataclass
class Sensor:
    ident: int
    planty: str
    addr: str

    _temperature: Optional[int] = field(init=False, default=None)
    _illuminance: Optional[int] = field(init=False, default=None)
    _moisture: Optional[int] = field(init=False, default=None)
    _conductivity: Optional[int] = field(init=False, default=None)

    def handle_adv(self, data):
        hdr, data = unpack_le16(data)
        device_id, data = unpack_le16(data)
        packet_id, data = unpack_u8(data)

        if (hdr >> 4) & 1: # read away embedded mac
            _, data = extract(data, 6)
        
        if (hdr >> 5) & 1: # read away capability fields
            _, data = unpack_u8(data)

        while len(data) > 3:
            payload_id, data = unpack_le16(data)
            payload_len, data = unpack_u8(data)
            payload, data = extract(data, payload_len)

            self._handle_payload(payload_id, payload)
    
    def _handle_payload(self, payload_id, payload):
        if payload_id == 0x1004:
            (temp,) = T_STRUCT.unpack(payload)
            self._temperature = temp/10
        elif payload_id == 0x1007:
            (self._illuminance,) = ILL_STRUCT.unpack(payload + b'\x00')
        elif payload_id == 0x1008:
            self._moisture = payload[0]
        elif payload_id == 0x1009:
            (self._conductivity,) = CND_STRUCT.unpack(payload)
        else:
            return

        self._maybe_submit()
    
    def _maybe_submit(self):
        if self._temperature is None or self._illuminance is None or self._moisture is None or self._conductivity is None:
            return

        info = {
            "light": self._illuminance,
            "temp": self._temperature,
            "soil_moist": self._moisture,
            "soil_ec": self._conductivity,
        }

        self._temperature = None
        self._illuminance = None
        self._moisture = None
        self._conductivity = None
        
        requests.post(f"{self.planty}/plant/{self.ident}/ingest", json=info)
        print("Submit", self.ident, info)


@dataclass
class ScanDelegate(DefaultDelegate):
    sensors: Dict[str, Sensor]

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if not isNewData:
            return

        sensor = self.sensors.get(dev.addr.upper())

        if not sensor:
            return

        data = dev.getValue(22)

        if not data:
            return

        uuid16, data = unpack_le16(data)

        if uuid16 == 0xFE95:
            sensor.handle_adv(data)
        else:
            print("unknown vendor")

with open(sys.argv[1]) as f:
    conf = yaml.safe_load(f)

sensors = {}

for sensor_conf in conf["sensors"]:
    sensor = Sensor(sensor_conf["id"], conf["planty"], sensor_conf["mac"])
    sensors[sensor.addr] = sensor

scanner = Scanner().withDelegate(ScanDelegate(sensors))

scanner.scan(None, passive=True)