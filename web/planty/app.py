from flask import (
    Flask,
    render_template,
    redirect,
    url_for,
    send_from_directory,
    request,
)
from flask_migrate import Migrate
from flask_wtf import FlaskForm
from flask_bootstrap import Bootstrap5
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, ValidationError
import requests
from datetime import datetime, timedelta
import os
import os.path
from urllib.request import urlretrieve, build_opener, install_opener, HTTPHandler
import math
import rrdtool
from collections import defaultdict

from planty.db import db, Plant


class Plantbook:
    def __init__(self, app):
        self._app = app
        self._token = None
        self._token_expires_at = None

    def login(self):
        if not self._token or (
            self._token_expires_at and self._token_expires_at < datetime.now()
        ):
            self._login()

    def _login(self):
        url = "https://open.plantbook.io/api/v1/token/"
        data = {
            "grant_type": "client_credentials",
            "client_id": app.config["PLANTBOOK_CLIENT_ID"],
            "client_secret": app.config["PLANTBOOK_CLIENT_SECRET"],
        }
        try:
            result = requests.post(url, data=data)
            result.raise_for_status()
        except (
            requests.exceptions.Timeout,
            requests.exceptions.TooManyRedirects,
            requests.exceptions.HTTPError,
            requests.exceptions.RequestException,
        ) as err:
            print("plantbook login error", err)
            raise Exception()

        data = result.json()
        print(data)
        self._token = data["access_token"]
        self._token_expires_at = datetime.now() + timedelta(
            seconds=data["expires_in"] - 60
        )

    def get_plant(self, pid):
        self.login()

        url = f"https://open.plantbook.io/api/v1/plant/detail/{requests.utils.quote(pid)}/"

        try:
            headers = {"Authorization": "Bearer {}".format(self._token)}
            result = requests.get(url, headers=headers)
            result.raise_for_status()
        except (
            requests.exceptions.Timeout,
            requests.exceptions.TooManyRedirects,
            requests.exceptions.HTTPError,
            requests.exceptions.RequestException,
        ) as err:
            print("plantbook login error", err)
            raise Exception()

        return result.json()


app = Flask(__name__)
app.config.from_envvar("PLANTY_CFG")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["BOOTSTRAP_SERVE_LOCAL"] = True
db.init_app(app)
migrate = Migrate(app, db)
bootstrap = Bootstrap5(app)
plantbook = Plantbook(app)


class MyHTTP(HTTPHandler):
    def http_request(self, req):
        plantbook.login()
        req.headers["Authorization"] = "Bearer {}".format(plantbook._token)
        return super().http_request(req)

opener = build_opener(MyHTTP())
install_opener(opener)

@app.route("/")
def overview():
    plants = Plant.query.all()
    return render_template("overview.html", plants=plants)


class NewPlantForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    plantbook_id = StringField("Plantbook ID", validators=[DataRequired()])
    submit = SubmitField()

    def validate_plantbook_id(form, field):
        try:
            plantbook.get_plant(form.plantbook_id.data)
        except:
            raise ValidationError("Invalid plantbook id")


def update_plantbook_info(plant):
    info = plantbook.get_plant(plant.plantbook_id)

    plant.species = info.get("display_pid")
    plant.max_light = info.get("max_light_lux")
    plant.min_light = info.get("min_light_lux")
    plant.max_temp = info.get("max_temp")
    plant.min_temp = info.get("min_temp")
    plant.max_soil_moist = info.get("max_soil_moist")
    plant.min_soil_moist = info.get("min_soil_moist")
    plant.max_soil_ec = info.get("max_soil_ec")
    plant.min_soil_ec = info.get("min_soil_ec")

    plant_dir = os.path.join(app.config["ASSET_DIR"], str(plant.id))
    if not os.path.exists(plant_dir):
        os.mkdir(plant_dir)
    icon_path = os.path.join(plant_dir, "icon.jpg")
    #urlretrieve(info["image_url"], icon_path)
    #'max_soil_moist': 60, 'min_soil_moist': 20, 'max_soil_ec': 2000, 'min_soil_ec': 100, 'image_url': 'https://objectstorage.ap-sydney-1.oraclecloud.com/n/sdyd5yr3jypo/b/plant-img/o/haemanthus%20albiflos.jpg'}


@app.route("/plant/new", methods=["GET", "POST"])
def new_plant():
    form = NewPlantForm()

    if form.validate_on_submit():
        plant = Plant()
        form.populate_obj(plant)
        db.session.add(plant)
        db.session.commit()

        update_plantbook_info(plant)
        db.session.add(plant)
        db.session.commit()
        return redirect(url_for("show_plant", id=plant.id))

    return render_template("new-plant.html", form=form)


@app.route("/plant/<int:id>")
def show_plant(id):
    plant = Plant.query.get(id)
    duration = request.args.get("duration", "7d")

    print("duration", duration)
    if duration not in ["1d", "7d", "14d", "30d", "1y"]:
        return redirect(url_for("show_plant", id=id))

    chart_data = rrd_fetch(plant, duration)
    return render_template("show-plant.html", plant=plant, chart_data=chart_data, duration=duration)


class EditPlantForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    plantbook_id = StringField("Plantbook ID", validators=[DataRequired()])
    submit = SubmitField()

    def validate_plantbook_id(form, field):
        try:
            plantbook.get_plant(form.plantbook_id.data)
        except:
            raise ValidationError("Invalid plantbook id")


@app.route("/plant/<int:id>/edit", methods=["GET", "POST"])
def edit_plant(id):
    plant = Plant.query.get(id)
    form = EditPlantForm(obj=plant)

    if form.validate_on_submit():
        form.populate_obj(plant)
        update_plantbook_info(plant)
        db.session.add(plant)
        db.session.commit()

        return redirect(url_for("show_plant", id=plant.id))

    return render_template("edit-plant.html", form=form)


@app.route("/plant/<int:id>/icon")
def plant_img(id):
    return send_from_directory(
        app.config["ASSET_DIR"], os.path.join(str(id), "icon.jpg")
    )


def rrd_path(plant):
    plant_dir = os.path.join(app.config["ASSET_DIR"], str(plant.id))
    rrd_path = os.path.join(plant_dir, "history.rrd")
    return rrd_path


def ensure_rrd(plant):
    path = rrd_path(plant)
    if os.path.exists(path):
        return

    rrdtool.create(
        path,
        "--step",
        "10m",
        "DS:light:GAUGE:20m:U:U",
        "DS:temp:GAUGE:20m:U:U",
        "DS:soil_moist:GAUGE:20m:0:100",
        "DS:soil_ec:GAUGE:20m:U:U",
        "DS:battery:GAUGE:20m:0:100",
        "RRA:AVERAGE:0.5:10m:1d",
        "RRA:AVERAGE:0.5:1h:10d",
        "RRA:AVERAGE:0.5:1d:10y",
        "RRA:MIN:0.5:10m:1d",
        "RRA:MIN:0.5:1h:10d",
        "RRA:MIN:0.5:1d:10y",
        "RRA:MAX:0.5:10m:1d",
        "RRA:MAX:0.5:1h:10d",
        "RRA:MAX:0.5:1d:10y",
    )


def update_rrd(plant):
    path = rrd_path(plant)

    values = [
        str(int(plant.last_updated.timestamp())),
    ]

    if plant.last_light is None:
        values.append("U")
    else:
        values.append(str(plant.last_light))

    if plant.last_temp is None:
        values.append("U")
    else:
        values.append(str(plant.last_temp))

    if plant.last_soil_moist is None:
        values.append("U")
    else:
        values.append(str(plant.last_soil_moist))

    if plant.last_soil_ec is None:
        values.append("U")
    else:
        values.append(str(plant.last_soil_ec))

    if plant.last_battery is None:
        values.append("U")
    else:
        values.append(str(plant.last_battery))

    rrdtool.update(
        path, "-t", "light:temp:soil_moist:soil_ec:battery", ":".join(values)
    )


def rrd_fetch(plant, dur):
    ensure_rrd(plant)
    path = rrd_path(plant)

    result = rrdtool.fetch(path, "AVERAGE", "-s", f"now-{dur}")
    start, end, step = result[0]
    ds = result[1]
    rows = result[2]

    t = start
    ret = defaultdict(list)
    for row in rows:
        for idx, val in enumerate(row):
            ret[ds[idx]].append({"x": datetime.fromtimestamp(t).isoformat(), "y": val})
        t += step

    return ret


@app.route("/plant/<int:id>/ingest", methods=["POST"])
def plant_ingest(id):
    plant = Plant.query.get(id)

    info = request.json

    plant.last_updated = datetime.now()
    plant.last_light = info.get("light")
    plant.last_temp = info.get("temp")
    plant.last_soil_moist = info.get("soil_moist")
    plant.last_soil_ec = info.get("soil_ec")
    if info.get("battery"):
        plant.last_battery = info.get("battery")

    ensure_rrd(plant)

    db.session.add(plant)
    db.session.commit()
    update_rrd(plant)

    return "okay"


@app.template_filter()
def relative_datetime(value):
    dt = datetime.now() - value
    print(repr(dt))

    if dt.total_seconds() < 60:
        return "now"
    elif dt.total_seconds() < 60 * 60:
        return f"{math.ceil(dt.total_seconds() / 60)}m"
    elif dt.total_seconds() < 24 * 60 * 60:
        return f"{math.ceil(dt.total_seconds() / (60 * 60))}h"
    else:
        return value.strftime("%d.%m.%y")
