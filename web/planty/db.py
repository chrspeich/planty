from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Plant(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    # Common Infos
    # ============
    # Our name for this plant
    name = db.Column(db.String(80), nullable=False)
    # Plant id in plantbook
    plantbook_id = db.Column(db.String(250))
    # Species name
    species = db.Column(db.String(250))

    # Last Values
    # ===========
    last_updated = db.Column(db.DateTime)
    last_light = db.Column(db.Integer)
    last_temp = db.Column(db.Float)
    last_soil_moist = db.Column(db.Integer)
    last_soil_ec = db.Column(db.Integer)
    last_battery = db.Column(db.Integer)

    # Values from open plantbook
    # ==========================
    max_light = db.Column(db.Integer)
    min_light = db.Column(db.Integer)

    # in deg celsius
    max_temp = db.Column(db.Integer)
    min_temp = db.Column(db.Integer)

    # in percent (0-100)
    max_soil_moist = db.Column(db.Integer)
    min_soil_moist = db.Column(db.Integer)

    # in uS/cm
    max_soil_ec = db.Column(db.Integer)
    min_soil_ec = db.Column(db.Integer)
