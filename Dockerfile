FROM python:3.9

RUN pip install pipenv
RUN apt-get update && apt-get install --no-install-recommends -y librrd-dev

COPY . /src/planty
WORKDIR /src/planty/web

RUN pipenv install --skip-lock
RUN pipenv run pip install gunicorn[gevent]

EXPOSE 8000
CMD ["pipenv", "run", "gunicorn", "-k", "gevent", "-w", "4", "-b", "0.0.0.0", "planty.app:app"]
